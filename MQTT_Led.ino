#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_NeoPixel.h>

// Expose Espressif SDK functionality - wrapped in ifdef so that it still
// compiles on other platforms
#ifdef ESP8266
#ifdef __cplusplus
extern "C" {
#endif// __cplusplus

#include "user_interface.h"

#ifdef __cplusplus
}
#endif // __cplusplus
#endif

#include "MQTT_Led.h"
#include "receiver.h"
#include "addr_util.h"

#define PIXEL_PIN    2    // Digital IO pin connected to the NeoPixels.
#define PIXEL_COUNT  5    // nb of leds



WiFiClient espClient;
PubSubClient client(espClient);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800);


void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

void setup_wifi() {
	
	#define STATION_IF 0x00
	#define SOFTAP_IF 0x01
	uint8_t sta_mac [6];
	uint8_t sofap_mac[6];
	char hostmane[ ] = "ESP-JBLB";
	
	if (wifi_station_set_hostname ( hostmane)) {
		Serial.println ("New hostname set ");
	}
	
	delay(10);
	// We start by connecting to a WiFi network
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(wifi_ssid);
	
	WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  // Call Espressif SDK functionality - wrapped in ifdef so that it still
  // compiles on other platforms
#ifdef ESP8266
  Serial.print("wifi_station_get_hostname: ");
  Serial.println(wifi_station_get_hostname());
  if (wifi_get_macaddr(STATION_IF, sta_mac)) {
	Serial.println("Got Mac for STATION_IF");
	// print MAC 
    for( int i=0; i<6; i++ ) {
		  Serial.print( sta_mac[i], HEX );
		  Serial.print( i < 5 ? ":" : "" );
	}
	Serial.println();
  
	}
  if (wifi_get_macaddr(SOFTAP_IF, sofap_mac )) {
	Serial.println("Got Mac for SOFTAP_IF");
	print_mac(sofap_mac);
	}

#endif
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    // If you do not want to use a username and password, change next line to
    // if (client.connect("ESP8266Client")) {
    // if (client.connect("ESP8266Client", mqtt_user, mqtt_password)) {
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("ping/#");      
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

long lastMsg = 0;

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > 1000) {
    lastMsg = now;
  }
}


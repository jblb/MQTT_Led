/*
 * varius utility for adress print 
 */

#include "addr_util.h"

void print_mac(byte *mymac){
// print MAC 
  for( int i=0; i<6; i++ ) {
    Serial.print( mymac[i], HEX );
    Serial.print( i < 5 ? ":" : "" );
  }
  Serial.println();
  }
  
